#pragma config(Motor,  port4,           Lift,          tmotorServoStandard, openLoop)
#pragma config(Motor,  port2,           Right,    tmotorNormal, openLoop)
#pragma config(Motor,  port3,           Left,     tmotorNormal, openLoop)
#pragma config(Motor,  port5,           Grasp,     tmotorNormal, openLoop, reversed)


#define GRASP_SPEED 20
#define DROP_LIMIT 15
//^can be changed to something larger

task main()  {			//TASK MAIN
	while(true) {
		motor[Right] = abs(vexRT[Ch2]) > DROP_LIMIT? -vexRT[Ch2] : 0; //left stick vertical axis
		motor[Left]  = abs(vexRT[Ch3]) > DROP_LIMIT? -vexRT[Ch3] : 0; //right stick vertical axis

	  motor[Lift] = vexRT[Btn5U] == 1? GRASP_SPEED : vexRT[Btn5D] == 1? -GRASP_SPEED : 0; //Lift
		motor[Grasp] = vexRT[Btn6U] == 1? GRASP_SPEED : vexRT[Btn6D] == 1? -GRASP_SPEED : 0; //GRASP
	}
}  